<?php

	define('DB_NAME', 'reservation');
	define('DB_USER', 'root');
	define('DB_PASSWORD', 'admin');
	define('DB_HOST', 'localhost');

	/*Connection to the databaase*/
	$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);

	//Tests that a good connection can be established or not
	if(!$link)
	{
		die('Could not connect: ' . mysqli_connect_errno());
	}

	//$db_selected = mysql_select_db(DB_NAME, $link);

	/*Connecting to particular database*/
	$db_select = mysqli_select_db($link, DB_NAME);

	if(!$db_select)
	{
		die('Can\'t use: ' . mysqli_error($link));
	}
?>