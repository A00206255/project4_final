<!DOCTYPE HTML>
<html lang="en">
	<head>
		<title>Movies</title>
		<link rel="stylesheet" type="text/css" href="css/main.css"/>
	</head>
	<body>
		<header>
			<h1>Movie Reservation</h1>	
		</header>
		
		<div id = "banner"><img width = "100%" src = "images/banner.png"></div>
		
		<hr>
		
		<nav>
			<div id="menu">
			<ul class = "navbar">
				<li>
					<a href="r/r_page.html">Data Analytics</a>
				</li>
				
				<li><a href="movies.php">Movies</a>
					<ul>
						<li>
							<a href = "glass.php">Glass</a>
						</li>
						<li>
							<a href = "logan.php">Logan</a>
						</li>
						<li>
							<a href = "overlord.php">Overlord</a>
						</li>
						<li>
							<a href = "mi6.php">Mission: Impossible - Fallout</a>
						</li>
						<li>
							<a href = "avengers.php">Avengers: Infinity War</a>
						</li>
					</ul>
				</li>
				
				<li>
					<a href="reservation.php">Reservations</a>
				</li>				
			</ul>
			</div>

		</nav>
		<br>
		<section>
			<table cellspacing = "0" cellpadding = "0" class = "movies">
				<tr>
					<td align = "center" colspan ="5"><a class = "movie_link" href = "glass.php">Glass</a></td>
				</tr>

				<tr>
					<td width="200px" rowspan = "5"><img width = "150px" height = "200px" src = "images/glass.jpg"></td>
					<td class = "titles">Director</td>
					<td>M.Night Shyamalan</td>
					<td> <div class="css-input">12:00</div></td>
					<td> <div class="css-input">18:00</div></td>
				</tr>
				<tr>
					<td class = "titles" rowspan = "4" colspan = "1">Starring</td>
					<td class = "actor">Bruce Willis</td>
					<td><div class="css-input">14:00</div></td>
					<td><div class="css-input">20:00</div></td>
				</tr>
				<tr>
					<td class = "actor">Samuel L. Jackson</td>
				</tr>
				<tr>
					<td class = "actor">James McAvoy</td>
					<td><div class="css-input">16:00</div></td>
					<td><div class="css-input">22:00</div></td>
				</tr>
					
			</table>
			<br>			
			<table cellspacing = "0" cellpadding = "0" class = "movies">
				<tr>
					<td align = "center" colspan ="5">Logan</td>
				</tr>

				<tr>
					<td width="200px" rowspan = "5"><img width = "150px" height = "200px" src = "images/logan.jpg"></td>
					<td class = "titles">Director</td>
					<td>James Mangold</td>
					<td> <div class="css-input">12:00</div></td>
					<td> <div class="css-input">18:00</div></td>
				</tr>
				<tr>
					<td class = "titles" rowspan = "4" colspan = "1">Starring</td>
					<td class = "actor">Hugh Jackman</td>
					<td><div class="css-input">14:00</div></td>
					<td><div class="css-input">20:00</div></td>
				</tr>
				</tr>
				<tr>
					<td class = "actor">Patrick Stewart</td>
				</tr>
				<tr>
					<td class = "actor">Dafne Keen</td>
					<td><div class="css-input">16:00</div></td>
					<td><div class="css-input">22:00</div></td>
				</tr>
					
			</table>
			<br>			
			<table cellspacing = "0" cellpadding = "0" class = "movies">
				<tr>
					<td align = "center" colspan ="5">Overlord</td>
				</tr>

				<tr>
					<td width="200px" rowspan = "5"><img width = "150px" height = "200px" src = "images/overlord.jpg"></td>
					<td class = "titles">Director</td>
					<td>Julius Avery</td>
					<td> <div class="css-input">12:00</div></td>
					<td> <div class="css-input">18:00</div></td>
				</tr>
				<tr>
					<td class = "titles" rowspan = "4" colspan = "1">Starring</td>
					<td class = "actor">Jovan Adepo</td>
					<td><div class="css-input">14:00</div></td>
					<td><div class="css-input">20:00</div></td>
				</tr>
				<tr>
					<td class = "actor">Jacob Anderson</td>
				</tr>
				<tr>
					<td class = "actor">Iain De Caestecker</td>
					<td><div class="css-input">16:00</div></td>
					<td><div class="css-input">22:00</div></td>
				</tr>
					
			</table>
			<br>			
			<table cellspacing = "0" cellpadding = "0" class = "movies">
				<tr>
					<td align = "center" colspan ="5">Mission: Impossible - Fallout</td>
				</tr>

				<tr>
					<td width="200px" rowspan = "5"><img width = "150px" height = "200px" src = "images/mi6.jpg"></td>
					<td class = "titles">Director</td>
					<td>Christopher McQuarrie</td>
					<td> <div class="css-input">12:00</div></td>
					<td> <div class="css-input">18:00</div></td>
				</tr>
				<tr>
					<td class = "titles" rowspan = "4" colspan = "1">Starring</td>
					<td class = "actor">Tom Cruise</td>
					<td><div class="css-input">14:00</div></td>
					<td><div class="css-input">20:00</div></td>
				</tr>
				<tr>
					<td class = "actor">Rebecca Ferguson</td>
				</tr>
				<tr>
					<td class = "actor">Henry Cavill</td>
					<td><div class="css-input">16:00</div></td>
					<td><div class="css-input">22:00</div></td>
				</tr>
					
			</table>
			<br>			
			<table cellspacing = "0" cellpadding = "0" class = "movies">
				<tr>
					<td align = "center" colspan ="5">Averngers: Infinity War</td>
				</tr>

				<tr>
					<td width="200px" rowspan = "5"><img width = "150px" height = "200px" src = "images/avengers.jpg"></td>
					<td class = "titles">Director</td>
					<td>Anthony/Joe Russo</td>
					<td> <div class="css-input">12:00</div></td>
					<td> <div class="css-input">18:00</div></td>
				</tr>
				<tr>
					<td class = "titles" rowspan = "4" colspan = "1">Starring</td>
					<td class = "actor">Robert Downey Jr</td>
					<td><div class="css-input">14:00</div></td>
					<td><div class="css-input">20:00</div></td>
				</tr>
				<tr>
					<td class = "actor">Chris Evans</td>
				</tr>
				<tr>
					<td class = "actor">Josh Brolin</td>
					<td><div class="css-input">16:00</div></td>
					<td><div class="css-input">22:00</div></td>
				</tr>
					
			</table>
		</section>
		<footer>
			<p style="text-align: center;"> 
			</p>

		</footer>
	</body>
</html>
