
//JavaScript function to only allow numbers to be entered
function isNumberKey(evt)
  {
	var e = evt || window.event; // for trans-browser compatibility
	var charCode = e.which || e.keyCode;                        
	if (charCode > 31 && (charCode < 47 || charCode > 57))
	return false;
	
	if (e.shiftKey) return false;
	
	return true;
 }
 
//JavaScript function to only allow letters and spaces to be entered
 function isAlfa(evt) 
 {
	evt = (evt) ? evt : window.event;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	
	if (charCode > 32 && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) 
	{
		return false;
	}
	return true;
}

function returnBack()
{
	window.location = 'movies.php';
}