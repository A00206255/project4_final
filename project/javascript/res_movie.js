var $ = function(id) 
{
	return document.getElementById(id); 
} 

var getHTTPObject = function() 
{
	var xhr = false; 
	if (window.XMLHttpRequest) 
	{ 
		xhr = new XMLHttpRequest(); 
	}
	else if (window.ActiveXObject) 
	{
		try 
		{ 
			xhr = new ActiveXObject("Msxml2.XMLHTTP"); 
		}
		catch(e) 
		{
			try 
			{ 
				xhr = new ActiveXObject("Microsoft.XMLHTTP"); 
			}
			catch(e) 
			{ 
				xhr = false; 
			} 
		}
	}
	return xhr;
}


var getWeatherURL = function(url, urlJSON) 
{
	var request = getHTTPObject(); 
	if (request) 
	{ 
		request.onreadystatechange = function() 
		{ 
			parseResponse(request);
		}
		
		
		request.open("GET", url, true);
		request.send(null)
	}

	var requestJSON = getHTTPObject(); 	
	
	if (requestJSON) 
	{
		requestJSON.onreadystatechange = function() 
		{ 
			parseJSON(requestJSON);
		}

		requestJSON.open("GET", urlJSON, true);
		requestJSON.send(null)
	}			

}
		
/*Funciton specifically to parse XML table and first graph.*/
var parseResponse = function(request)
{	
	if (request.readyState == 4) 
	{
		if (request.status == 200 || request.status == 304)
		{			
			console.log(request.responseXML);
			
			var data = request.responseXML;

			var sunrise = data.getElementsByTagName("sun")[0].getAttribute("rise");
			sunrise = sunrise.substring(11, 19);
			console.log(sunrise);

			var sunset = data.getElementsByTagName("sun")[0].getAttribute("set");
			sunset = sunset.substring(11, 19);
			console.log(sunset);
						
			var time = data.getElementsByTagName("time")[0].getAttribute('from');
			
			var tempAccum = 0;
		
			var times = data.getElementsByTagName("time");
			//console.log("Length " + times.length);
			
			
			/*Accumulating the temperature values for the 5 days*/
			for(var t = 0; t < times.length; t++)
			{
				tempAccum += parseFloat(data.getElementsByTagName("temperature")[t].getAttribute('value'));
				tempAccum = tempAccum.toFixed(2);
				tempAccum = parseFloat(tempAccum);
				tempAccum -= 273.15;		/*Convert from Kelvin to Celsius*/
				tempAccum = tempAccum.toFixed(2);				
				tempAccum = parseFloat(tempAccum);
			}
			
			/*Calculating overall average*/
			var tempAvg = tempAccum/ times.length;
			tempAvg = tempAvg.toFixed(2);
			console.log(tempAvg);

			var windVal = 0;
			var windDay1 = 0;
			var windDay2 = 0;
			var windDay3 = 0;
			var windDay4 = 0;
			var windDay5 = 0;
			
			/*Gathering daily wind speeds*/
 			for(var w  = 0; w < times.length; w++)
			{
				if(w >= 0 && w <= 7)
				{
					windDay1 += parseFloat(data.getElementsByTagName("windSpeed")[w].getAttribute('mps'));
					windDay1 = windDay1.toFixed(2);	
					windDay1 = parseFloat(windDay1);
				}
				else if(w >= 8 && w <= 15)
				{
					windDay2 += parseFloat(data.getElementsByTagName("windSpeed")[w].getAttribute('mps'));
					windDay2 = windDay2.toFixed(2);	
					windDay2 = parseFloat(windDay2);
				}
				else if(w >= 16 && w <= 23)
				{
					windDay3 += parseFloat(data.getElementsByTagName("windSpeed")[w].getAttribute('mps'));
					windDay3 = windDay3.toFixed(2);	
					windDay3 = parseFloat(windDay3);
				}
				else if(w >= 24 && w <= 31)
				{
					windDay4 += parseFloat(data.getElementsByTagName("windSpeed")[w].getAttribute('mps'));
					windDay4 = windDay4.toFixed(2);	
					windDay4 = parseFloat(windDay4);
				}
				else if(w >= 32 && w <= 39)
				{
					windDay5 += parseFloat(data.getElementsByTagName("windSpeed")[w].getAttribute('mps'));
					windDay5 = windDay5.toFixed(2);	
					windDay5 = parseFloat(windDay5);
				}
			}
			
			var wind = Array(5);
			
			/*Calculating average for each*/
			wind[0] = windDay1 / 8;
			wind[0] = windDay1.toFixed(2);	
			console.log(wind[0]);
			
			wind[1] = windDay2 / 8;
			wind[1] = windDay2.toFixed(2);	
			console.log(wind[1]);
			
			wind[2] = windDay3 / 8;
			wind[2] = windDay3.toFixed(2);	
			console.log(wind[2]);
			
			wind[3]= windDay4 / 8;
			wind[3]= windDay4.toFixed(2);	
			console.log(wind[3]);
			
			wind[4]= windDay5 / 8;
			wind[4]= windDay5.toFixed(2);	
			console.log(wind[4]);

			/*Span created for main <th> in table to span all across */
			var span = "";
			span="colspan=4";
			
			var span2 = "colspan=2";
			
			/*Variable used to later define blank rows and give them ID 'blank' */
			var blank = "";
			blank = "id = 'blank'";

			/*Address for sunrise and sunset icons*/
			var riseLink  = "'images/sunrise.png'";
			var setLink  = "'images/sunset.png'";
			
			
			/*Creating table to display weather*/
			var table =	"<th " +span+">Forecast for " + data.getElementsByTagName("name")[0].firstChild.nodeValue + 
			" ("+data.getElementsByTagName("country")[0].firstChild.nodeValue+")</th><tr " +blank+ " " + span2 +
			"> </tr><tr><td>Sunrise: " + sunrise + "<br><img src = " + riseLink + "></td><td " +span2+ ">Avg Temperature: " + 
			tempAvg + "°C</td><td>Sunset: " + sunset + "<br><img src = " + setLink + "></td></tr><tr " +blank+ " " + span2+ "></tr>";

			/*Appending subheadings onto table*/
			table += "<tr><th width = '25%'></th><th>Date</th><th>Temperature</th><th width = '22%'>Wind Speed</th></tr>";

			
			var days = new Array(5);
			var dateTime;
			
			var counter = 0;
			
			/*Loop created to get the dates using substring. This is to remove the time and display the date only.*/
			for (var i = 0; counter <= 4; counter++)
			{
				dateTime = times[i].getAttribute('from');

				/*Ensuring dates don't appear twice*/
				if(dateTime.substring(0, 10) != days[counter-1])
				{
					days[counter] = dateTime.substring(0, 10);
					i+=8; //Increment by 8 to ensure it moves onto a new day
				}
				/*console.log(days[counter]);*/
			}
			
			var imageLink = "";
			var l = 0;
			
			/*Loop to set up image icon links and then appending them to the table*/
			for (var i = 0; i < times.length; i+= 8) 
			{
				var symbol = times[i].getElementsByTagName("symbol")[0].getAttribute('var');
				imageLink = "http://openweathermap.org/img/w/" +symbol+ ".png";
			
				
				table += "<tr><td><img src = '" +imageLink + "'></td>";
				
				/*Displaying the temperature. Subtraction is made to convert into Degrees Celsius from Kelvin*/
				table += "<td>" + days[l] + "</td><td>" 
				+ (times[l].getElementsByTagName("temperature")[0].getAttribute('max') - 273.15).toFixed(2) +
				"°C</td><td>" +wind[l]+ " mps</td</tr>";

				l++;
			}
			
			document.getElementById("weather").innerHTML = table;
				
			var temps = new Array(5);
			var counter = 0;
			
			
			/*Temps obtained for purposes of displaying in radar chart*/
			for (var i = 0; i < 40 ; i+= 8) 
			{
				temps[counter] = data.getElementsByTagName("temperature")[counter].getAttribute('max');
				temps[counter] = parseFloat(temps[counter]);
				temps[counter] -= 273.15;
				temps[counter] = temps[counter].toFixed(2);
			
				counter++;
			}
						
			//create html elements
		//	var h2 = document.createElement("h2");
			//h2.appendChild(document.createTextNode(country));
			var weatherPanel = document.getElementById("weatherPanel1");
		
		
			makeChart(days, wind, temps);
		}
	//Parse data from XML and add to webpage 
	}
} 


/*Separate function for JSON parsing a line chart.*/
var parseJSON = function(request)
{	
	if (request.readyState == 4) 
	{
		if (request.status == 200 || request.status == 304)
		{			
			//console.log(request.responseText);
			
			var obJSON = JSON.parse(request.responseText);
			
			console.log("JASON" + obJSON.list[0].main.temp);
			
			var list = obJSON.list;
			
			//console.log("LIST " +list[0].main.humidity);
			
			var dates = new Array(10)
			var humidity = new Array(10);
			var tempKelvin = new Array(10);
			//console.log("LIST " +list[i].main.humidity);
			
			var i = 0;
			
			for(i = 0; i < list.length; i = i + 4)
			{
				dates[i/4] = list[i].dt_txt;
				humidity[i/4] = list[i].main.humidity;
				tempKelvin[i/4] = list[i].main.temp;
			}
			
			var ctx = document.getElementById('chartJSON');
			
			var myChart = new Chart(ctx, 
			{
				type: 'line',
				data: 
				{
					labels: [dates[0], dates[1], dates[2], dates[3], dates[4], dates[5], dates[6], dates[7], dates[8], dates[9]],
					datasets: [
					{
						label: 'Humidity',
						data: [humidity[0], humidity[1], humidity[2], humidity[3], humidity[4], humidity[5], humidity[6], humidity[7], humidity[8], humidity[9]],
						backgroundColor: "rgba(153,255,51,0.6)"
					}, 
					{
						label: 'Temperature in Kelvin',
						data: [tempKelvin[0], tempKelvin[1], tempKelvin[2], tempKelvin[3], tempKelvin[4], tempKelvin[5], tempKelvin[6], tempKelvin[7], tempKelvin[8], tempKelvin[9]],
						backgroundColor: "rgba(255,153,0,0.6)"
					}
					]
				}
			});



		}
	}
}

var makeChart = function(daysArray, windArray, tempArray)
{
	var ctx = document.getElementById("chart");
	
	//console.log(timesArray[0].getElementsByTagName("symbol").getAttribute("var"));
	
	var myChart = new Chart(ctx, 
	{
		type: 'radar',
		data: 
		{
			labels: [daysArray[0], daysArray[1], daysArray[2], daysArray[3], daysArray[4]], 
			datasets: 
			[
				{
					label: 'Wind Speed',
					backgroundColor: "rgba(153,255,51,0.4)",
					borderColor: "rgba(153,255,51,1)",
					data: [windArray[0], windArray[1], windArray[2], windArray[3], windArray[4]]
				}, 
				{
					label: 'Temperature',
					backgroundColor: "rgba(255,153,0,0.4)",
					borderColor: "rgba(255,153,0,1)",
					data: [tempArray[0], tempArray[1], tempArray[2], tempArray[3], tempArray[4],]
				}
			]
		}
	});
}


window.onload = function()
{
	$("searchButton").onclick = function()
	{
		var city = $("city").value; 
		console.log(city);

		var url = "http://api.openweathermap.org/data/2.5/forecast?q=" + city + "&mode=xml&appid=392212c0acd7987ba2fa0960280b516d";
		
		var urlJSON = "http://api.openweathermap.org/data/2.5/forecast?q=" + city + "&mode=json&appid=392212c0acd7987ba2fa0960280b516d";

		
		getWeatherURL(url, urlJSON); 
	} 
	$("city").focus(); 
}