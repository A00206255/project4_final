var $ = function(id) 
{
	return document.getElementById(id); 
} 

var getHTTPObject = function() 
{
	var xhr = false; 
	if (window.XMLHttpRequest) 
	{ 
		xhr = new XMLHttpRequest(); 
	}
	else if (window.ActiveXObject) 
	{
		try 
		{ 
			xhr = new ActiveXObject("Msxml2.XMLHTTP"); 
		}
		catch(e) 
		{
			try 
			{ 
				xhr = new ActiveXObject("Microsoft.XMLHTTP"); 
			}
			catch(e)
			{ 
				xhr = false; 
			} 
		}
	}
	return xhr;
}


var getMovie = function(url) 
{
	var request = getHTTPObject(); 
	if (request) 
	{ 
		request.onreadystatechange = function() 
		{ 
			parseResponse(request);
		}
		
		
		request.open("GET", url, true);
		request.send(null)
	}
}
		
/*Funciton specifically to parse XML table and first graph.*/
var parseResponse = function(request)
{	
	if (request.readyState == 4) 
	{
		if (request.status == 200 || request.status == 304)
		{			
			console.log(request.responseXML);
				
			var data = request.responseXML;
			
			var default_runtime = "120 mins";
			
			var title = data.getElementsByTagName("movie")[0].getAttribute("title");
			
			var genre = data.getElementsByTagName("movie")[0].getAttribute("genre");
			console.log(genre);

			var runtime = data.getElementsByTagName("movie")[0].getAttribute("runtime");
			console.log(runtime);
			
			if(runtime == "N/A")
			{
				runtime = default_runtime;
			}
			
			var director = data.getElementsByTagName("movie")[0].getAttribute("director");
			console.log(director);
			
			var actors = data.getElementsByTagName("movie")[0].getAttribute("actors");
			var act_split = actors.split(",", 4);
			console.log(act_split);
			//document.getElementById("demo").innerHTML = res;
			
			var synopsis = data.getElementsByTagName("movie")[0].getAttribute("plot");
			
			document.getElementById("title").innerHTML += title;
			document.getElementById("details").innerHTML += genre + " | " +runtime;
			document.getElementById("director").innerHTML += director;
			document.getElementById("actors").innerHTML +=  act_split[0] +" | "+act_split[1]+" | "+act_split[2]+" | "+act_split[3];
			document.getElementById("plot").innerHTML += synopsis;
			
		}
	//Parse data from XML and add to webpage 
	}
} 


window.onload = function()
{
	var movie = document.getElementsByTagName("title")[0].text;
	var res = movie.toLowerCase();
	console.log(res);
		
	var year = document.getElementsByTagName("input")[0].value;
	console.log(year);

	var url = "http://www.omdbapi.com/?t=" + res + "&y="+year+"&plot=short&r=xml&apikey=f5f3d2a6";
	
	getMovie(url); 
}