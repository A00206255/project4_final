<?php
	session_start(); 
	// Import PHPMailer classes 
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;
	
	/*Getting values from SESSION (validate.php) to ready the SQL insert*/
	$movie = $_SESSION['movieSession'];
	$ticket = $_SESSION['ticketSession'];
	$time = $_SESSION['timeSession'];
	$format = $_SESSION['formatSession'];
	$price = $_SESSION['priceSession'];
	
	/*SQL insert statement to be executed*/
	$sql = "Insert into reservation_det (movie_title, ticket_type, time_reserved, viewing_format, payment) VALUES ('$movie', '$ticket', '$time', '$format', '$price')";

	/*Looking to the database config file to get connection values from there ($link)*/
	include '../dbconfig/config.php';
	
	$query_run = mysqli_query($link, $sql);

	/*Attmept to run the insert query*/
	if($query_run)
	{	
		mysqli_close($link);

		header("Location: email.php");
	}
	else
	{		
		if(!mysqli_query($link, $sql))
		{
			die('Error: ' . mysqli_error($link));
		}
		echo '<script type = "text/javascript">alert("Insert Failed")</script>';				
	}	
		
	/*Connection to the databaase*/
	//$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
?>