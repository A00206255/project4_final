<?php
	session_start(); 
	//If insert is ssuccessful send a confirmation email
	
	// Import PHPMailer classes 
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	//Load Composer's autoloader
	require '../vendor/autoload.php';

	$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
	try 
	{
		//Server settings
		$mail->SMTPDebug = 1;                                 // Enable verbose debug output
		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = 'smtp.gmail.com';  					  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = 'kevin.mcquaide.bsc2@gmail.com';	  // SMTP username
		$mail->Password = 'buckfrost009';                     // SMTP password
		$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 465;                                    // SSl port to connect to for gmail hosted SMTP

		//Recipients
		$mail->setFrom('noreply@project.com');
		$mail->addAddress('kevin.mcquaide.bsc2@gmail.com');   		// Add a recipient

		//Message of confirmation
		$body = '<p>Movie Reserved: &nbsp;'.$_SESSION['movieSession'] .'</p>
		<p>Time Booked: &nbsp;'.$_SESSION['timeSession'].'</p>
		<p>Viewing In: &nbsp;'.$_SESSION['formatSession']. '</p>
		<p>Amount Payed: &euro;&nbsp;'. $_SESSION['priceSession'] .'</p>
		<p>Thank you for booking with us</p>';
		
		//Content
		$mail->isHTML(true);                                  // Set email format to HTML
		$mail->Subject = 'Your Reservation';
		$mail->Body    = $body;
		$mail->AltBody = strip_tags($body);

		$mail->send();
		header("Location: ../movies.php");

	} 
	catch (Exception $e) 
	{
		echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
	}

?>