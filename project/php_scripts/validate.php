<?php

if(isset($_POST['reservebtn']))
{	
	session_start();
	/*Getting hidden <p> value which
	contains the name of the movie*/
	$movie = $_POST['movie'];
	$_SESSION['movieSession'] = $movie;			//Session value created to allow other PHP files to access
		
	/*Values which will be used to detemine price for 
	tickets and viewing format depending on choice*/
	$ticket_price = 0;
	$viewing_price = 0;
	
	
	/*Get value from ti1cket drop down. 
	Checks that a proper selection was made*/
	if($_POST['ticketList'] == null)
	{
		header('Refresh:0.1; url='.$_SESSION['link']);
		echo '<script type = "text/javascript">alert("No ticket selection has been made")</script>';
	}
	else
	{
		$ticket= $_POST['ticketList'];
		$_SESSION['ticketSession'] = $ticket;
		
		/*If statement to evaluate the price charged for the ticket chosen*/
		if($ticket == "Child")
		{
			$ticket_price = 6.25;
		}
		else if($ticket == "Teen")
		{
			$ticket_price = 7.90;
		}
		else if($ticket == "Adult")
		{
			$ticket_price = 9.00;
		}
		else if($ticket == "Student")
		{
			$ticket_price = 7.50;
		}
		else if($ticket == "Senior")
		{
			$ticket_price = 7.00;
		}

		/*Get value from time radio buttons*/
		if(!empty($_POST['time']))
		{
			if($_POST["time"] == 1)
			{
				$time = "12:00";			
			}
			else if($_POST["time"] == 2)
			{
				$time = "14:00";
			}
			else if($_POST["time"] == 3)
			{
				$time = "16:00";
			}
			else if($_POST["time"] == 4)
			{
				$time = "18:00";
			}
			else if($_POST["time"] == 5)
			{
				$time = "20:00";
			}
			else
			{	
				$time = "22:00";
			}
			$_SESSION['timeSession'] = $time;
		}
		else
		{
			header('Refresh:0.1; url='.$_SESSION['link']);
			echo '<script type = "text/javascript">alert("No Selection has been made and submitted yet")</script>';
		}
		
		/*Get value from viewing format list*/
		if(!empty($_POST['format']))
		{
			$format = $_POST['format'];
			/*If anything other than the standard 2D format is chosen, an additional price is charged*/
			if($format == "3d" || $format == "IMAX")
			{
				$viewing_price = 5.00;
			}

			$_SESSION['formatSession'] = $format;
		}
			
		$admission_price = $ticket_price + $viewing_price;	//Total price of admission
		$_SESSION['priceSession'] = $admission_price;		//Session variable made so it can be accessed in separate files


		header("Location: ../payment.php");
	}
}


?>

