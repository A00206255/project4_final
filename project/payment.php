<?php
	/*Takes the code in the database configuration file and executes it 
	  allowing us to establish connection to the database straight away*/
	require 'dbconfig/config.php';

?>

<!DOCTYPE HTML>
<html lang="en">
	<head>
		<title>Make Payment</title>
		<link rel="stylesheet" type="text/css" href="css/pay.css"/>
		<script src="javascript/payment.js"></script>

		<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
		<meta content="utf-8" http-equiv="encoding">
	</head>
	<body>
		<form id ="pay_form" action = "payment.php" method = "post" accept-charset=utf-8>
				
				<h1>Credit Card Payment</h1>
				<h2>Amount Owed:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&euro;<?php 		//Small PHP section to display the price owed
	
				session_start(); 			//Session started to access price session variables in validate.php 
				echo $_SESSION['priceSession']; 
				
				$numlength = strlen((string)$_SESSION['priceSession']);
				
				/*When the amount owed is displayed on screen, values with zero (0) at the end are missing those values
				This If statement looks at the length and add on the extra zeroes for display*/
				if($_SESSION['priceSession'] != 5.25)	//Avoids a case where '€6.250' is displayed on screen
				{
					if($numlength == 2)
					{
						echo '.00';
					}
					else if($numlength == 3)
					{
						echo 0;
					}
					else if($numlength == 4)
					{
						echo 0;
					}
				}
				?></h2>
				
				<p>Please complete your payment by entering your credit card details below</p>
				<p>
					<label class="label" for="time">Card Number:*</label>
					<input onkeypress="return isNumberKey(event)" type = "text" id = "card" placeholder = "Enter 16-digit credit card number" value = "" name = "cardNo"/>
				</p>
				<p>
					<label class="label" for="time">Name on Card:*</label>
					<input type = "text" onkeypress="return isAlfa(event)" id = "name" placeholder = "Enter name on the card" value = "" name = "cardName"/>
				</p>
				<p>
					<label class="label" for="time">Card Expiry Date:*</label>
					 <select>
						<option>1</option>      
						<option>2</option>      
						<option>3</option>      
						<option>4</option>      
						<option>5</option>      
						<option>6</option>      
						<option>7</option>      
						<option>8</option>      
						<option>9</option>      
						<option>10</option>      
						<option>11</option>      
						<option>12</option>      
					</select> 
					 <select>
						<option>18</option>      
						<option>19</option>      
						<option>20</option>      
						<option>21</option>      
						<option>22</option>      
						<option>23</option>      
						<option>24</option>      
						<option>25</option>      
						<option>26</option>      
						<option>27</option>      
						<option>28</option>      
						<option>29</option>      
						<option>30</option>      
						<option>31</option> 
					</select> 
				</p>
				<p>
					<label class="label" for="time">CVC:*</label>
					<input onkeypress="return isNumberKey(event)" id = "card" placeholder = "Enter 3-digit CVC on back of card" name = "cvc"/>
				</p>
				<button id = "btn" name="paybtn" type = "submit" value = "pay_button" value="Reserve">Reserve</button>
				<button id = "btn" name="" type = "reset" value = "" value="Reserve">Reset</button>
				<button id = "btn" name="return" value = "" value="Reserve">Cancel Payment</button>

				<?php
					if(isset($_POST['paybtn']))
					{
						$_cardNo = $_POST['cardNo'];
						$_name = $_POST['cardName'];
						$_cvc = $_POST['cvc'];
						
						/*Code to get the number of digits in the card number and cvc*/
						$numlength = strlen((string)$_cardNo);
						$cvclength = strlen((string)$_cvc);
						
						/*If statement to validate the card number (16 digits), the CVC (3 digits) and that thet name isn't null*/
						if($numlength != 16)
						{
							echo '<script type = "text/javascript">alert("Credit Card number must be 16 digits")</script>';
						}
						else if($_name == null)
						{
							echo '<script type = "text/javascript">alert("Enter name")</script>';		
						}
						else if($cvclength != 3)
						{
							echo '<script type = "text/javascript">alert("CVC must be the 3 digit security number found on the back of the card")</script>';		
						}
						else
						{
							header("Location: php_scripts/reserve.php");
						}
					}
					else if(isset($_POST['return']))
					{
							header("Location: movies.php");						
					}
				?>
		</form>					
	</body>
</html>
