<!DOCTYPE HTML>
<html lang="en">
	<head>
		<title>Reservations</title>
		<link rel="stylesheet" type="text/css" href="css/reserve_list.css"/>
	</head>
	<body>
		<header>
			<h1>Movie Reservation</h1>	
		</header>
		
		<div id = "banner"><img width = "100%" src = "images/banner.png"></div>
		
		<hr>
		
		<nav>
			<div id="menu">
			<ul class = "navbar">
				<li>
					<a href="r/r_page.html">Data Analytics</a>
				</li>
				
				<li><a href="movies.php">Movies</a>
					<ul>
						<li>
							<a href = "glass.php">Glass</a>
						</li>
						<li>
							<a href = "logan.php">Logan</a>
						</li>
						<li>
							<a href = "overlord.php">Overlord</a>
						</li>
						<li>
							<a href = "mi6.php">Mission: Impossible - Fallout</a>
						</li>
						<li>
							<a href = "avengers.php">Avengers: Infinity War</a>
						</li>
					</ul>
				</li>
				
				
				<li>
					<a href="reservation.php">Reservations</a>
				</li>			
			</ul>
			</div>

		</nav>
		<br>
		<section>
			<!--**************************************
				Separate table for displaying all reservations according to times booked
			**************************************
			-->
			<?php
				include 'dbconfig/config.php';
				
				/*Takes the code in the database configuration file and executes it 
				allowing us to establish connection to the database straight away*/
				
				echo "<table id = 'reservations'><tr> <td valign='top'>";

				$sql = "SELECT res_id, movie_title, ticket_type FROM reservation_det WHERE time_reserved = '12:00'";
				$result = $link->query($sql);

				echo "Viewings booked for 12 O'Clock (12:00)\n.";
				if ($result->num_rows > 0) {
					echo "<table id = 'list'><tr><th>ID</th><th>Title</th><th>Ticket Type</th></tr>";
					// output data of each row
					while($row = $result->fetch_assoc()) 
					{
						echo "<tr><td>" . $row["res_id"]. "</td><td>" . $row["movie_title"]. "</td><td>" . $row["ticket_type"]. "</td></tr>";
					}
					echo "</table>";
				} 
				else 
				{
					echo "0 results";
				}
				
				echo "</td><td valign='top'>";

				echo "Viewings booked for 2 O'Clock (14:00)\n.";
				
				$sql = "SELECT res_id, movie_title, ticket_type FROM reservation_det WHERE time_reserved = '14:00'";
				$result = $link->query($sql);

				if ($result->num_rows > 0) {
					echo "<table id = 'list'><tr><th>ID</th><th>Title</th><th>Ticket Type</th></tr>";
					// output data of each row
					while($row = $result->fetch_assoc()) 
					{
						echo "<tr><td>" . $row["res_id"]. "</td><td>" . $row["movie_title"]. "</td><td>" . $row["ticket_type"]. "</td></tr>";
					}
					echo "</table>";
				} 
				else 
				{
					echo "0 results";
				}				
//				$link ->close();
				
				echo "</td></tr>";
				
				
				echo "<tr><td valign = 'top'>";
				
				
				$sql = "SELECT res_id, movie_title, ticket_type FROM reservation_det WHERE time_reserved = '14:00'";
				$result = $link->query($sql);

				echo "Viewings booked for 4 O'Clock (16:00)\n";
				if ($result->num_rows > 0) {
					echo "<table  id = 'list'><tr><th>ID</th><th>Title</th><th>Ticket Type</th></tr>";
					// output data of each row
					while($row = $result->fetch_assoc()) 
					{
						echo "<tr><td>" . $row["res_id"]. "</td><td>" . $row["movie_title"]. "</td><td>" . $row["ticket_type"]. "</td></tr>";
					}
					echo "</table>";
				} 
				else 
				{
					echo "0 results";
				}
				
				echo "</td><td valign='top'>";

				echo "Viewings booked for 6 O'Clock (18:00)\n.";
				
				$sql = "SELECT res_id, movie_title, ticket_type FROM reservation_det WHERE time_reserved = '18:00'";
				$result = $link->query($sql);

				if ($result->num_rows > 0) {
					echo "<table id = 'list'><tr><th>ID</th><th>Title</th><th>Ticket Type</th></tr>";
					// output data of each row
					while($row = $result->fetch_assoc()) 
					{
						echo "<tr><td>" . $row["res_id"]. "</td><td>" . $row["movie_title"]. "</td><td>" . $row["ticket_type"]. "</td></tr>";
					}
					echo "</table>";
				} 
				else 
				{
					echo "0 results";
				}

				echo "<tr><td valign = 'top'>";
				
				
				$sql = "SELECT res_id, movie_title, ticket_type FROM reservation_det WHERE time_reserved = '20:00'";
				$result = $link->query($sql);

				echo "Viewings booked for 8 O'Clock (20:00)\n";
				if ($result->num_rows > 0) {
					echo "<table id = 'list'><tr><th>ID</th><th>Title</th><th>Ticket Type</th></tr>";
					// output data of each row
					while($row = $result->fetch_assoc()) 
					{
						echo "<tr><td>" . $row["res_id"]. "</td><td>" . $row["movie_title"]. "</td><td>" . $row["ticket_type"]. "</td></tr>";
					}
					echo "</table>";
				} 
				else 
				{
					echo "0 results";
				}
				
				echo "</td><td valign='top'>";

				echo "Viewings booked for 10 O'Clock (22:00)\n.";
				
				$sql = "SELECT res_id, movie_title, ticket_type FROM reservation_det WHERE time_reserved = '22:00'";
				$result = $link->query($sql);

				if ($result->num_rows > 0) {
					echo "<table  id = 'list'><tr><th>ID</th><th>Title</th><th>Ticket Type</th></tr>";
					// output data of each row
					while($row = $result->fetch_assoc()) 
					{
						echo "<tr><td>" . $row["res_id"]. "</td><td>" . $row["movie_title"]. "</td><td>" . $row["ticket_type"]. "</td></tr>";
					}
					echo "</table>";
				} 
				else 
				{
					echo "0 results";
				}
				
				
				echo "</td></tr></table>";
			?>
			
			<table cellspacing = "0" cellpadding = "0" class = "res">
				<tr>
				</tr>
			</table>
			
		</section>
		<footer>

		</footer>
	</body>
</html>
