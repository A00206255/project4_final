#install.packages("dbConnect", type = "source")
#install.packages("RMySQL")
#install.packages("sqldf")
install.packages("forecast")

library(RMySQL)
library(dbConnect)
library(sqldf)
library(forecast)
library(chron)

#Establishing connection to database
con = dbConnect(MySQL(), user = 'root', password = 'admin', dbname = 'reservation', host = 'localhost')

#Listing all tables in that database
dbListTables(con) 

#Direct way to get a table and store in a vector
reservation <- dbReadTable(con, 'reservation_det')

#Number of rows in data frame
rows <- nrow(reservation)

#Counting how many tickets were sold at each time
count_12 <- sum(reservation$time_reserved == "12:00")
count_14 <- sum(reservation$time_reserved == "14:00")
count_16 <- sum(reservation$time_reserved == "16:00")
count_18 <- sum(reservation$time_reserved == "18:00")
count_20 <- sum(reservation$time_reserved == "20:00")
count_22 <- sum(reservation$time_reserved == "22:00")

res_times <- c(count_12, count_14, count_16, count_18, count_20, count_22)

sqldf('SELECT DISTINCT reservation_det.time_reserved FROM reservation_det ')

times <- reservation$time_reserved  #Get all time values
times <- unique(times)              #Remove duplicate times
times <- sort(times)                #Sort times

res <- data.frame(times, res_times)

plot(ts(res$times,frequency = 2),ylab='Number of people reserved',xlab='Time',main='Chart of Bookings')
auto.arima(ts(res$times,frequency = 2))


###################### SIMPLE LINEAR REGRESSION CODE ############################

time1 <- reservation$time_reserved  #Get all time values
time1 <- unique(times)              #Remove duplicate times
time1 <- sort(times)                #Sort times

#Converts into time form that R can understand
x <- as.POSIXct(reservation$time_reserved,format="%H:%M")

plot(x, reservation$payment, xlab = "Time", ylab = "count")
#plot(res$times, res$res_times, xlab = "Time", ylab = "count")

model <- lm(payment~x, data = reservation)
summary(model) 

#Plot linear regression model for all prices against time
plot(x, reservation$payment)
abline(model)


reserveation_baseprices <- cbind(reservation) #Separate frame for prices not including add on's

for(i in 1:nrow(reserveation_baseprices)) #Loop to go find IMAX/3D viewers and remove those add on prices
{
  if(reserveation_baseprices[i, 5] == "IMAX" ||reserveation_baseprices[i, 5] == "3d") 
  {
    reserveation_baseprices[i, 6] <- reserveation_baseprices[i, 6] - 2
  }
}

#Plot a linear regression on base prices (no add-ons for IMAX and 3D)
base_price_model <- lm(reserveation_baseprices$payment~x, data = reserveation_baseprices)

plot(x, reserveation_baseprices$payment, xlab = "Reservation times", ylab = "Prices")
abline(base_price_model)







######################## UNIX #############################

#Converts into time form that R can understand
time_booked <- as.POSIXct(reservation$time_reserved,format="%H:%M")
price_booked <- reservation$payment

#Converts times to UNIX times
unix_times <- as.numeric(as.POSIXct(x))

#Normalises the UNIX values into a range of 0-1 values
normalised_times <- (unix_times - min(unix_times))/(max(unix_times)-min(unix_times))

#Normalises the prices into a range of 0-1 values
normalised_prices <- (price_booked - min(price_booked))/(max(price_booked)-min(price_booked))

#Model of normalised prices against normalised times booked
model <- lm(normalised_prices~normalised_times)
plot(model)



#New times to predict
timespred <- c("23:00", "23:45")
times_to_predict <- as.POSIXct(timespred, format="%H:%M")

#Converts predicting times to UNIX times
unix_newTimes <- as.numeric(as.POSIXct(times_to_predict))

#Normalising the new times to be predicted
normalised_pred <- (unix_newTimes - min(unix_times))/(max(unix_times)-min(unix_times))

#Making preditctions for new times entered using linear model
predictions <- predict(model, normalised_pred)

#(x * (max(y, na.rm=TRUE) - min(y, na.rm=TRUE)) + min(y, na.rm=TRUE))
denorm_prices <- (predictions * (max(normalised_prices) - min(normalised_prices)) + min(normalised_prices))


data(mtcars)

## 75% of the sample size

## set the seed to make your partition reproductible
set.seed(123)
train_ind <- sample(seq_len(nrow(mtcars)), size = round(nrow(mtcars)*0.8))

train <- mtcars[train_ind, ]
test <- mtcars[-train_ind, ]

train <- mtcars[1:26,]
test <- mtcars[27:nrow(mtcars),]










