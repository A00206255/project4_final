<?php
	/*Takes the code in the database configuration file and executes it 
	  allowing us to establish connection to the database straight away*/
	require 'dbconfig/config.php';
	
	session_start();
	
	$_SESSION['link'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
?>

<!DOCTYPE HTML>
<html lang="en">
	<head>
		<title>Avengers: Infinity War</title>
		<link rel="stylesheet" type="text/css" href="css/movie.css"/>
	</head>
	<body>
		<header>
			<h1>Movie Reservation</h1>	
			<script src="javascript/api.js"></script>
		</header>
		
		<div id = "banner"><img width = "100%" src = "images/banner.png"></div>
		
		<hr>
		
		<nav>
			<div id="menu">
			<ul class = "navbar">
				<li>
					<a href="r/r_page.html">Data Analytics</a>
				</li>
				
				<li><a href="movies.php">Movies</a>
					<ul>
						<li>
							<a href = "glass.php">Glass</a>
						</li>
						<li>
							<a href = "logan.php">Logan</a>
						</li>
						<li>
							<a href = "overlord.php">Overlord</a>
						</li>
						<li>
							<a href = "mi6.php">Mission: Impossible - Fallout</a>
						</li>
						<li>
							<a href = "avengers.php">Avengers: Infinity War</a>
						</li>
					</ul>
				</li>
				
				
				<li>
					<a href="reservation.php">Reservations</a>
				</li>			
			</ul>
			</div>

		</nav>
		<br>
		<input type="hidden" name="year" value="2018">
		<section>
			<!--**************************************
				Table to detail the movie
			**************************************
			-->
			<table cellspacing = "0" cellpadding = "0" class = "movies">
				<tr>
				</tr>

				<tr>
					<td style="padding-right: 22px;" width="170px" rowspan = "6"><img id = "poster" src = "images/avengers.jpg"></td>
					<td id = "title" align = "left" colspan ="5"></td>
				</tr>
				<tr>
					<td id = "details" align = "left" ></td>
				</tr>
				<tr>
					<td id = "director" align = "left" >Director: </td>
				</tr>
				<tr>
					<td id = "actors" align = "left" >Actors: </td>
				</tr>
				<tr>
					<td id = "synopsis" align = "left" >SYNOPSIS</td>
				</tr>
				<tr>
					<td id = "plot" align = "left" ></td>
				</tr>
				<tr>
					<td colspan = "2"><hr style = "width : 99%;"><br><td>
				</tr>
			</table>
			
			<!--**************************************
				Separate table for selecting reservation details
			**************************************
			-->
				<table cellspacing = "0" cellpadding = "0" class = "booking_details">
				<tr>
					<form class="details_form" action = "php_scripts/validate.php" method = "post">
						<td style="width: 20%;">
						
							<!-- Special line to hold the value of the movie. Required for PHP to 
							get the name of the movie. Hidden away so cannot be seen on the interface-->
							<p id = "hide"><input type="hidden" name="movie" value="Avengers: Infinity War"></p>
							
							<fieldset id = "ticket_field">
								

								<label class="label" for="time">Select Ticket Type:</label>
								<select name = "ticketList" style = "width: 60%;">
									<option value="">Please Select</option>
								 <optgroup label="Minor">
									<option value="Child">Child</option>
									<option value="Teen">Teen</option>
								  </optgroup>
								  <optgroup label="Other">
									<option value="Adult">Adult</option>
									<option value="Student">Student</option>
									<option value="Senior">Senior</option>
								  </optgroup>
								</select>
							</fieldset>
						</td>
						<td>
							<fieldset id = "viewing_field">
								<div>
									<label class="label" for="time">Select Viewing Time:</label>
									<select style = "width: 70%;" name="time" size="3">
										<option value="1">12:00</option>
										<option value="2">14:00</option>
										<option value="3">16:00</option>
										<option value="4">18:00</option>
										<option value="5">20:00</option>
										<option value="6">22:00</option>
									</select>									
							</div>
							</fieldset>
						</td>
						
						<td>
							<fieldset id = "time_field">
								<label class="label">Choose Viewing Format:</label>									
									<div>
										<div class="radio">
											<label for="time-0">
											<input name="format" id="time-0" value="2d" checked="checked" type="radio">Standard (2d)</label>
										</div>

										<div class="radio">
											<label for="time-1">
											<input name="format" id="time-1" value="3d" type="radio">3D</label>
										</div>
										
										<div class="radio">
											<label for="time-2">
											<input name="format" id="time-2" value="IMAX" type="radio">IMAX</label>
										</div>
									</div>
							</fieldset>
						</td>
						<td>
							<button id = "reserve_button" name="reservebtn" type = "submit" value="Reserve">Reserve</button>
						</td>
					
					</form>
				</tr>
			</table>
		</section>
		<footer>
		</footer>
	</body>
</html>
